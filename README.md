# Installation

Please make sure you have homebridge installed ie.
```sh
sudo npm install -g --unsafe-perm homebridge
```

After this, install this plugin ie.
```sh
sudo npm install -g
```
