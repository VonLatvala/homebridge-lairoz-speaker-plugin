var Service, Characteristic;
const request = require('request');
const url = require('url');

module.exports = function (homebridge) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  homebridge.registerAccessory("homebridge-lairoz-speaker", "LAIROZ-SPEAKER", LAIROZ_SPEAKER);
};

function LAIROZ_SPEAKER(log, config) {
  this.log = log;
  this.service = "Speaker";
  this.name = config['name'];
  this.muteEndpoint = url.parse(config['muteEndpoint']).href;
  this.volEndpoint = url.parse(config['volEndpoint']).href;
}

LAIROZ_SPEAKER.prototype = {
  getServices: function () {
    const me = this;

    let informationService = new Service.AccessoryInformation();
    informationService
      .setCharacteristic(Characteristic.Manufacturer, "Axel Latvala")
      .setCharacteristic(Characteristic.Model, "homebridge-lairoz-speaker")
      .setCharacteristic(Characteristic.SerialNumber, "133-950-003");

    me.log('creating Speaker');
    me.log('WARN: Dirtily disguising "Speaker" as "Lightbulb"');

    let speakerService = new Service.Lightbulb(me.name);

    me.log('... adding mute control');
    me.log('WARN: Dirtily disguising "Mute" as "On"');

    speakerService
      .getCharacteristic(Characteristic.On)
      .on('get', this.getMuteCharacteristic.bind(this))
      .on('set', this.setMuteCharacteristic.bind(this));

    me.log('... adding volume control');
    me.log('WARN: Dirtily disguising "Volume" as "Brightness"');

    speakerService
      .getCharacteristic(Characteristic.Brightness)
      .on('get', this.getVolumeCharacteristic.bind(this))
      .on('set', this.setVolumeCharacteristic.bind(this));


    this.informationService = informationService;
    this.speakerService = speakerService;
    return [informationService, speakerService];
  },
  getMuteCharacteristic: function (next) {
    const me = this;
    request({
        url: me.muteEndpoint,
        method: 'GET',
    },
    function (error, response, body) {
      if (error) {
        if(!response) {
          me.log('STATUS: ENDPOINT UNAVAILABLE');
        } else {
            me.log('STATUS: ' + (response.statusCode || 'ENDPOINT UNAVAILABLE'));
        }
        me.log(error.message || 'Unknown');
        return next(error);
      }
      // XXX: the negation is because we are disguised as 'On' instead of 'Mute'
      return next(null, !JSON.parse(body).muted);
    });
  },
  setMuteCharacteristic: function (mute, next) {
    const me = this;
    const action = !mute ? 'mute' : 'unmute';
    request({
      url: me.muteEndpoint+'?action='+action,
      body: null,
      method: 'GET'
    },
    function (error, response) {
      if (error) {
        if(!response) {
          me.log('STATUS: ENDPOINT UNAVAILABLE');
        } else {
            me.log('STATUS: ' + (response.statusCode || 'ENDPOINT UNAVAILABLE'));
        }
        me.log(error.message);
        return next(error);
      }
      return next();
    });
  },
  getVolumeCharacteristic: function (next) {
    const me = this;
    request({
        url: me.volEndpoint,
        method: 'GET',
    },
    function (error, response, body) {
      if (error) {
        if(!response) {
          me.log('STATUS: ENDPOINT UNAVAILABLE');
        } else {
            me.log('STATUS: ' + (response.statusCode || 'ENDPOINT UNAVAILABLE'));
        }
        me.log(error.message || 'Unknown');
        return next(error);
      }
      return next(null, JSON.parse(body).vol);
    });
  },
  setVolumeCharacteristic: function (level, next) {
    const me = this;
    request({
      url: me.volEndpoint+'?action=set&val='+level,
      body: null,
      method: 'POST',
      headers: {'Content-type': 'application/json'}
    },
    function (error, response) {
      if (error) {
        me.log('STATUS: ' + (response.statusCode || 'ENDPOINT UNAVAILABLE'));
        me.log(error.message || 'Unknown');
        return next(error);
      }
      return next();
    });
  }
};
